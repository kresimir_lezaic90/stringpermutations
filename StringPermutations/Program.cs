﻿using System;
using System.Collections.Generic;

namespace StringPermutations
{
    class Program
    {
        static void Main(string[] args)
        {
            String input;
            String query;
 
            while(true)
            {
                Console.Write("Input String: ");
                input = Console.ReadLine();

                if (input.Length > 10)
                {
                    Console.WriteLine("String length must be less than 10 due to memory overflow issues!");
                    continue;
                }

                if (input.Length <= 1)
                {
                    Console.WriteLine("String length must be atleast 2 due to common sense!");
                    continue;
                }

                Console.Write("Print on screen (Y/N): ");
                query = Console.ReadLine();

                if (query.ToUpper().Contains("Y") || query.ToUpper().Contains("N"))
                    break;
                else
                {
                    Console.WriteLine("Y/N pl0x!");
                    continue;
                }
            }
            
            Permutator p = new Permutator(input);

            List<String> output = p.getAllPermutations();

            if(query.Contains("Y"))
            {
                Console.WriteLine(Environment.NewLine + "PERMUTATIONS :");
                output.ForEach(Console.WriteLine);
            }

            Console.WriteLine(Environment.NewLine + "STATS: ");
            Console.WriteLine("Length of input string: " + input.Length);
            Console.WriteLine("Number of Permutations: " + output.Count);
            Console.WriteLine("Time: " + p.runtime()/1000.0 + " s");
            Console.WriteLine("Time: " + p.runtime() + " ms");
            Console.WriteLine("Time: " + p.runtimeInUs() + " us");
            Console.WriteLine("Time: " + p.runtimeInNs() + " ns");
            Console.WriteLine("Time: " + p.runtimeInPs() + " ps");
            Console.WriteLine("Time: " + p.runtimeInTicks() + " ticks");
            Console.WriteLine(Environment.NewLine);

            return;

        }
        
    }
}
