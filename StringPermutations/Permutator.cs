﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace StringPermutations
{
    public class Permutator
    {
        private String input;
        private List<String> output;

        private Stopwatch clock;

        public Permutator()
        {
            input = "";
            output = new List<String>();
            clock = new Stopwatch();
        }

        public Permutator(String input)
        {
            this.input = input;
            output = new List<String>();
            clock = new Stopwatch();
        }

        public List<String> getAllPermutations()
        {
            clock.Start();
            char[] charArr = input.ToCharArray();
            int maxDepth = charArr.Length - 1;
            getPermutation(charArr, 0, maxDepth);

            clock.Stop();

            return output;
        }

        private void getPermutation(char[] list, int depth, int maxDepth)
        {
            if (depth == maxDepth)
            {
                output.Add(new string(list));
            }
            else
                for (int i = depth; i <= maxDepth; i++)
                {
                    swap(ref list[depth], ref list[i]);
                    getPermutation(list, depth + 1, maxDepth);
                    swap(ref list[depth], ref list[i]);
                }
        }

        private void swap (ref char x, ref char y)
        {
            if (x == y) return;

            x ^= y;
            y ^= x;
            x ^= y;
        }

        public double runtime()
        {
            return clock.ElapsedMilliseconds;
        }

        public double runtimeInTicks()
        {
            return clock.ElapsedTicks;
        }

        public double runtimeInUs()
        {
            return ((clock.ElapsedTicks * 1000000.0) / Stopwatch.Frequency);
        }

        public double runtimeInNs()
        {
            return ((clock.ElapsedTicks * 1000000000.0) / Stopwatch.Frequency) ; 
        }

        public double runtimeInPs()
        {
            return ((clock.ElapsedTicks * 1000000000000.0) / Stopwatch.Frequency);
        }
        
    }
}
